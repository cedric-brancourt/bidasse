// Licensed under the MIT license. See LICENSE file in the project root for details.
use native_tls::TlsStream;
use std::io::{Read, Write};
use std::net::TcpStream;

pub struct Selector {
    pub mbox: String,
    pub query: String,
}

pub struct SessionConfig {
    pub host: String,
    pub user: String,
    pub pass: String,
    pub port: u16,
}

pub fn init_session(config: SessionConfig) -> imap::Session<TlsStream<TcpStream>> {
    let tls = native_tls::TlsConnector::builder().build().unwrap();
    let client = imap::connect(
        (config.host.as_str(), config.port),
        config.host.as_str(),
        &tls,
    )
    .unwrap();
    client
        .login(config.user, config.pass)
        .map_err(|e| e.0)
        .unwrap()
}

pub fn get_messages<T: Read + Write>(
    session: &mut imap::Session<T>,
    selector: Selector,
) -> Vec<Vec<u8>> {
    session.select(selector.mbox).unwrap();
    let ids = session.search(selector.query).unwrap();
    let mut bodies = Vec::new();
    let messages = session.fetch(seq_set(ids), "BODY[]").unwrap();
    for message in messages.iter() {
        let body = message.body().unwrap().to_vec();
        //let body = std::str::from_utf8(body).unwrap().to_string();
        bodies.push(body)
    }
    bodies
}

fn seq_set(ids: std::collections::HashSet<u32>) -> String {
    ids.iter()
        .map(std::string::ToString::to_string)
        .collect::<Vec<String>>()
        .join(",")
}
