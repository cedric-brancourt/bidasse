// Licensed under the MIT license. See LICENSE file in the project root for details.
mod email;
mod imap;

use crate::imap::{Selector, SessionConfig};
#[macro_use]
extern crate clap;
use clap::{App, ArgMatches};

// Adapters for Clap argument parser
// This prevent application config to be coupled to the argument parser
impl<'a> From<&ArgMatches<'a>> for SessionConfig {
    fn from(args: &ArgMatches) -> Self {
        SessionConfig {
            host: args.value_of("host").unwrap().to_string(),
            user: args.value_of("user").unwrap().to_string(),
            pass: args.value_of("pass").unwrap().to_string(),
            port: 993,
        }
    }
}

impl<'a> From<&ArgMatches<'a>> for Selector {
    fn from(args: &ArgMatches) -> Self {
        Selector {
            mbox: args.value_of("mbox").unwrap().to_string(),
            query: args.value_of("query").unwrap().to_string(),
        }
    }
}

fn main() {
    let manifest = load_yaml!("../cli_manifest.yml");
    let matches = App::from_yaml(manifest).get_matches();

    let mut session = imap::init_session(SessionConfig::from(&matches));
    let messages = imap::get_messages(&mut session, Selector::from(&matches));
    println!("Found messages count: {}", &messages.iter().count());

    let mimetype = matches.value_of("mimetype").unwrap();
    let mut context = email::crypto_context();
    let attachments = email::attachments(&messages, mimetype, &mut context);
    println!("attachment count: {}", attachments.iter().count());

    let dir = matches.value_of("dir").unwrap();
    for attachment in attachments.iter() {
        println!("writing {}", attachment.name);
        attachment.write(&dir)
    }
}
