// Licensed under the MIT license. See LICENSE file in the project root for details.
use gpgme::{Context, Protocol};
use mailparse::ParsedMail;
use std::fs::File;
use std::io::Write;

#[derive(Debug, Clone)]
struct MailPart {
    mimetype: String,
    body: Vec<u8>,
    subparts: Vec<MailPart>,
    file_name: Option<String>,
}

fn parse(messages: &[Vec<u8>]) -> Vec<MailPart> {
    messages.iter().map(move |x| parse_email(x)).collect()
}

fn parse_email(message: &[u8]) -> MailPart {
    let parsed = mailparse::parse_mail(message).unwrap();
    MailPart::from(parsed)
}

impl<'a> From<ParsedMail<'a>> for MailPart {
    fn from(p: ParsedMail) -> MailPart {
        MailPart {
            mimetype: p.ctype.mimetype.to_string(),
            body: p.get_body_raw().unwrap(),
            subparts: p.subparts.into_iter().map(MailPart::from).collect(),
            file_name: p.ctype.params.get("name").cloned(),
        }
    }
}

#[derive(Debug, Clone)]
struct Encrypted {
    body: Vec<u8>,
}

fn is_encrypted(m: &MailPart) -> bool {
    m.mimetype == "multipart/encrypted"
}

pub fn crypto_context() -> Context {
    Context::from_protocol(Protocol::OpenPgp).unwrap()
}

impl Encrypted {
    pub fn decrypt(&self, crypto_context: &mut Context) -> Vec<u8> {
        let mut decrypted = Vec::new();
        crypto_context
            .decrypt(&mut self.body.clone(), &mut decrypted)
            .unwrap();
        decrypted
    }
}

impl From<&MailPart> for Encrypted {
    fn from(part: &MailPart) -> Self {
        let part = part
            .subparts
            .iter()
            .find(|s| s.mimetype == "application/octet-stream")
            .unwrap();
        let body = part.body.clone();
        Encrypted { body }
    }
}

fn get_attachments(message: &MailPart, mimetype: &str) -> Vec<Attachment> {
    let mut attachments = Vec::new();
    if message.mimetype == mimetype {
        attachments.push(Attachment::from(message))
    }
    for subpart in &message.subparts {
        attachments.extend(get_attachments(&subpart, mimetype))
    }
    attachments
}

#[derive(Debug, Clone)]
pub struct Attachment {
    pub name: String,
    pub content: Vec<u8>,
}

impl From<&MailPart> for Attachment {
    fn from(part: &MailPart) -> Self {
        let name = match &part.file_name {
            Some(name) => name.clone(),
            None => uuid::Uuid::new_v4().to_string(),
        };
        let content = part.body.clone();

        Attachment { name, content }
    }
}

impl Attachment {
    pub fn write(&self, dir: &str) {
        let mut file = File::create(format!("{}/{}", dir, self.name)).unwrap();
        let content = &self.content[..];
        file.write_all(content).unwrap()
    }
}

pub fn attachments(
    messages: &[Vec<u8>],
    mimetype: &str,
    crypto_ctx: &mut Context,
) -> Vec<Attachment> {
    let parsed = parse(messages);
    let (encrypteds, clear): (Vec<MailPart>, Vec<MailPart>) =
        parsed.into_iter().partition(|x| is_encrypted(x));

    let decrypteds: Vec<MailPart> = encrypteds
        .iter()
        .map(|x| {
            let encrypted = Encrypted::from(x).decrypt(crypto_ctx);
            parse_email(&encrypted)
        })
        .collect();

    let mut all = Vec::new();
    all.push(decrypteds);
    all.push(clear);
    all.iter()
        .flatten()
        .flat_map(|m| get_attachments(m, mimetype))
        .collect()
}
