# bidasse

Batch Imap Download AttachmentS for Secured Emails

A command line tool to download attachments from imap mailboxes with crypto support through gpgme.

## Dependencies

* gpgme
* openssl

## Install

### From sources

Get the sources:

```
git clone https://gitlab.com/cedric-brancourt/bidasse.git
```

You need rust toolchain:

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Compile:

```
cd bidasse/
cargo build --release
```

then copy `target/release/bidasse` somewhere in your path.

## Use

Will download all attachments from the provided `mimetype` in the output `dir`.

```
USAGE:
    bidasse [OPTIONS] --pass <pass> --user <user>

FLAGS:
        --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --dir <dir>              target directory for downloads [default: /tmp]
    -h, --host <host>            imap hostname [default: imap.gmail.com]
    -b, --mbox <mbox>            target imap mailbox [default: [Gmail]/Tous les messages]
    -m, --mimetype <mimetype>    target subpart mimetype [default: application/pdf]
    -p, --pass <pass>            imap password
    -q, --query <query>          an IMAP RFC compliant search query [default: SUBJECT "paie"]
    -u, --user <user>            imap username
```
